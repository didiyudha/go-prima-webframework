package user

import (
	"net/http"

	"github.com/labstack/echo"
)

func Routes(e *echo.Echo) {
	userRoute := e.Group("/user")
	userRoute.GET("", index)
}

func index(c echo.Context) error {
	return c.String(http.StatusOK, "user module")
}
