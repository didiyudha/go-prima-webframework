package main

import (
	"net/http"

	"github.com/labstack/echo"

	userModules "gitlab.com/didiyudha/go-prima-webframework/modules/user"
)

func main() {
	e := echo.New()

	userModules.Routes(e)

	e.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "Hello, World!")
	})

	e.Logger.Fatal(e.Start(":8888"))
}
